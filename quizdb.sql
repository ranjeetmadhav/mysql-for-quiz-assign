-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2021 at 07:56 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quizdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `participant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  `timetaken` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`participant_id`, `name`, `email`, `score`, `timetaken`) VALUES
(1, 'hhuhuhu', 'uiggg', 0, 0),
(2, 'hhuhuhu jjjo', 'uiggg jjjo', 0, 0),
(12, 'tfgf', 'ythg', 0, 0),
(13, 'Ranjeet Madhav', '@gmail.com', 0, 0),
(14, 'ranjeet', 'madgac', 0, 0),
(15, 'Ranjeet', 'RanjeetMadhav@gmail.com', 0, 0),
(16, 'Ranjeet majdgs', 'jhhjjh', 0, 0),
(17, 'ranjeet', 'djiddjk', 0, 0),
(18, '', '', 0, 0),
(19, '', '', 0, 0),
(20, 'iiu', 'jkjk', 0, 0),
(21, 'gugu', 'jggu', 0, 0),
(22, 'ranjeet Madhav', '@gmail.com', 0, 0),
(23, '', '', 0, 0),
(24, 'Ranjeet Madhav', '@gmail.com', 0, 0),
(25, 'Ranjeet', '@gmail.com', 0, 0),
(26, 'Ranjeet', 'madgac', 0, 0),
(27, 'Ranjeet', '@gmail.com', 0, 0),
(28, 'Ranjeet', 'jidjis', 0, 0),
(29, 'Ranjeet Madhav', 'ranjeet@gmail.com', 0, 0),
(30, 'Ranjeet Madhav', '', 0, 0),
(31, 'Ranjeet', '@gmail.com', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `questid` int(11) NOT NULL,
  `question1` varchar(251) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`questid`, `question1`) VALUES
(1, 'A'),
(2, 'E'),
(3, 'k'),
(4, 'l'),
(5, 'm'),
(6, 'n');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `ansid` int(11) NOT NULL,
  `ans1` varchar(100) NOT NULL,
  `ans2` varchar(251) NOT NULL,
  `Ans` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`ansid`, `ans1`, `ans2`, `Ans`) VALUES
(1, 'Apple', '', 1),
(2, 'Elephant', '', 2),
(3, 'kite', '', 3),
(4, 'lion', '', 4),
(5, 'Monkey', '', 5),
(6, 'Nest', '', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`questid`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`ansid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `participant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `questid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `ansid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
